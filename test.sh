#!/bin/bash

JAVA="java";
JAVA_RUN="${JAVA} -cp out/production/hh_points/"
GEN="${JAVA_RUN} ru/h32/hh/points/Generate";
GEN_PARAMS="10000 10000 100"
SIMPLEST_SOLVER="${JAVA_RUN} ru/h32/hh/points/Main simplest";
SOLVER="${JAVA_RUN} ru/h32/hh/points/Main preparata";
AMOUNT=$1

if [ ! ${AMOUNT} -gt 0 ]; then
 echo "Use test.sh <numberOfTests>";
 exit;
fi

cleanup() {
    rm -f simplest.out.txt
    rm -f solver.out.txt
    rm -f points.txt
}

test_gen() {
    RUN="${GEN} random ${GEN_PARAMS}";
    ${RUN} > points.txt;
}

run_simplest() {
    ${SIMPLEST_SOLVER} points.txt > simplest.out.txt
}

run_testing_solver() {
    ${SOLVER} points.txt > solver.out.txt
}

test_equality() {
    SIMPLEST=`cat simplest.out.txt`
    TESTING=`cat solver.out.txt`

    if [ "${SIMPLEST}" == "${TESTING}" ];
    then
        echo "${i} OK ${SIMPLEST}=${TESTING}"
    else
        echo "Error!"
        echo "Simplest=" ${SIMPLEST}
        echo "Solver  =" ${TESTING}
#        exit;
    fi
}

#=========================================

i="0"
while [ $i -lt ${AMOUNT} ]
do
    cleanup;
    test_gen;
    run_simplest;
    run_testing_solver;
    test_equality;

    i=$[$i+1];
done

cleanup;