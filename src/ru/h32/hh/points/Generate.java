package ru.h32.hh.points;

import ru.h32.hh.points.generators.GeneratorFactory;
import ru.h32.hh.points.generators.GeneratorNotFoundException;
import ru.h32.hh.points.generators.IncorrectParametersException;
import ru.h32.hh.points.generators.PointsGenerator;

import java.util.Arrays;

/**
 * Генерирует набор точек с помощью generator в stdout
 */
public class Generate {

    public static void main(String[] args) {

        Point p1 = new Point(0,0);
        Point p2 = new Point(0,0);

        if (args.length >= 1) {

            String generatorName = args[0];

            try {
                PointsGenerator g = GeneratorFactory.get(generatorName);
                g.setParameters(Arrays.copyOfRange(args, 1, args.length));
                for (Point point : g.getPoints()) {
                    System.out.print(point.x);
                    System.out.print(" ");
                    System.out.println(point.y);
                }

            } catch (GeneratorNotFoundException e) {
                System.out.printf("Generator %s not found", generatorName);
            } catch (IncorrectParametersException e) {
                System.out.printf("Strange params for generator");
            }

        } else {
            System.out.println("Use <generator> <generator_params>");
        }
    }

}
