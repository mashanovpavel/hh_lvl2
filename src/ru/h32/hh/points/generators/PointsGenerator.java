package ru.h32.hh.points.generators;

import ru.h32.hh.points.Point;

import java.util.ArrayList;
import java.util.Map;

public interface PointsGenerator {

    public ArrayList<Point> getPoints();

    public void setParameters(Map<String, Integer> parameters) throws IncorrectParametersException;

    public void setParameters(String parameters) throws IncorrectParametersException;

    public void setParameters(String[] parameters) throws IncorrectParametersException;
}
