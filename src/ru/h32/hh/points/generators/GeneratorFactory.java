package ru.h32.hh.points.generators;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class GeneratorFactory {

    protected static Map<String, Class> supportedGenerators = null;

    public static PointsGenerator get(String type) throws GeneratorNotFoundException {

        if(type.equals("random")) {
            return new RandomGenerator();
        }

        throw new GeneratorNotFoundException();
    }
}
