package ru.h32.hh.points.generators;

import ru.h32.hh.points.Point;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class RandomGenerator implements PointsGenerator {

    protected int sizeX = 0;
    protected int sizeY = 0;
    protected int amount = 0;

    @Override
    public ArrayList<Point> getPoints() {

        ArrayList<Point> points = new ArrayList<Point>();
        Random rg = new Random();

        for (int count = 0; count < this.amount; count++) {
            Point p =  new Point(rg.nextInt(this.sizeX), rg.nextInt(this.sizeY));
            points.add(p);
        }

        return points;
    }

    @Override
    public void setParameters(Map<String, Integer> parameters) throws IncorrectParametersException {
        if (parameters.containsKey("sizeX") && parameters.containsKey("sizeY") && parameters.containsKey("amount")) {
            this.sizeX = parameters.get("sizeX");
            this.sizeY = parameters.get("sizeY");
            this.amount = parameters.get("amount");
        } else {
            throw new IncorrectParametersException();
        }
    }

    @Override
    public void setParameters(String parameters) throws IncorrectParametersException {
        String[] params = parameters.split(" ");
        this.setParameters(params);
    }

    public void setParameters(String[] params) throws IncorrectParametersException {
        Map<String, Integer> paramsMap = new HashMap<String, Integer>();

        if (params.length == 3) {
            try {
                paramsMap.put("sizeX", Integer.parseInt(params[0]));
                paramsMap.put("sizeY", Integer.parseInt(params[1]));
                paramsMap.put("amount", Integer.parseInt(params[2]));
            } catch (NumberFormatException e) {
                throw new IncorrectParametersException();
            }
        } else if(params.length == 2) {
            try {
                paramsMap.put("sizeX", Integer.parseInt(params[0]));
                paramsMap.put("sizeY", Integer.parseInt(params[0]));
                paramsMap.put("amount", Integer.parseInt(params[1]));
            } catch (NumberFormatException e) {
                throw new IncorrectParametersException();
            }
        }

        this.setParameters(paramsMap);
    }
}
