package ru.h32.hh.points.solvers;

import ru.h32.hh.points.Point;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/**
 * Разделяй-и-влавствуй
 */
public class PreparataSolver implements Solver {

    protected static final int TRIVIAL_SEARCH_THRESHOLD = 4;

    protected double currentMinimal = Double.MAX_VALUE;

    protected Point[] points = null;
    protected Point[] mergeBuffer = null;
    protected ComparatorY comparatorY = new ComparatorY();

    protected void updateCurrentAnswer(Point p1, Point p2) {
        double dist = getDistance(p1, p2);

        if (dist < currentMinimal) {
            currentMinimal = dist;
        }
    }

    @Override
    public double solve(Point[] points) {

        this.currentMinimal = Double.MAX_VALUE;
        this.points = points;
        this.mergeBuffer = new Point[points.length];

        //Подготовка
        Arrays.parallelSort(this.points, new ComparatorXY());

        //Запуск рекурсии
        solveFor(0, this.points.length - 1);

        return currentMinimal;
    }

    void solveFor(int left, int right) {
//        System.out.println("solveFor(from=" + left + ",to=" + right + ")...");

        //Предел рекурсии
        if (right - left <= TRIVIAL_SEARCH_THRESHOLD) {
            for (int i = left; i <= right; i++)
                for (int j = i + 1; j <= right; j++) {
                    this.updateCurrentAnswer(this.points[i], this.points[j]);
                }

            Arrays.sort(this.points, left, right + 1, new ComparatorY());

            return;
        }

        //split
        int middle = (left + right) / 2;
        int midx = this.points[middle].x;

        this.solveFor(left, middle);
        this.solveFor(middle + 1, right);

        merge_inplace(left, middle, right);

        //join
        ArrayList<Point> t = new ArrayList<Point>();
        for (int i = left; i <= right; i++)
            if (Math.abs(this.points[i].x - midx) < currentMinimal) {
                for (int j = t.size() - 1; (j >= 0) && ((this.points[i].y - t.get(j).y) < currentMinimal); j--) {
                    updateCurrentAnswer(this.points[i], t.get(j));
                }
                t.add(this.points[i]);
            }
    }

    protected void merge_inplace(int lowerIndex, int middle, int higherIndex) {

        System.arraycopy(points, lowerIndex, mergeBuffer, lowerIndex, higherIndex + 1 - lowerIndex);
        int i = lowerIndex;
        int j = middle + 1;
        int k = lowerIndex;
        while (i <= middle && j <= higherIndex) {
            if (comparatorY.compare(mergeBuffer[i], mergeBuffer[j]) <= 0) {
                points[k] = mergeBuffer[i];
                i++;
            } else {
                points[k] = mergeBuffer[j];
                j++;
            }
            k++;
        }
        while (i <= middle) {
            points[k] = mergeBuffer[i];
            k++;
            i++;
        }
    }

    protected static double getDistance(Point p1, Point p2) {
        return Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
    }
}

class ComparatorXY implements Comparator<Point> {
    @Override
    public int compare(Point o1, Point o2) {
        if (o1.x > o2.x)
            return 1;
        if (o1.x < o2.x)
            return -1;
        if (o1.y > o2.y)
            return 1;
        if (o1.y < o2.y)
            return -1;

        return 0;
    }
}

class ComparatorX implements Comparator<Point> {
    @Override
    public int compare(Point o1, Point o2) {
        if (o1.x > o2.x)
            return 1;
        if (o1.x < o2.x)
            return -1;

        return 0;
    }
}

class ComparatorY implements Comparator<Point> {
    @Override
    public int compare(Point o1, Point o2) {
        if (o1.y > o2.y)
            return 1;
        if (o1.y < o2.y)
            return -1;

        return 0;
    }
}
