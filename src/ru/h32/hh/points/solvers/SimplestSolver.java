package ru.h32.hh.points.solvers;

import ru.h32.hh.points.Point;

import java.util.ArrayList;

/**
 * Полный перебор
 */
public class SimplestSolver implements Solver{

    @Override
    public double solve(Point[] points) {

        if(points.length <= 1)
            return 0;

        long minimalDistanceQuad = Long.MAX_VALUE;

        for(int i = 0; i< points.length; i++) {
            for(int j = 0; j < points.length; j++) {
                if(i != j) {
                    long d = this.getDistance(points[i], points[j]);

                    if(d < minimalDistanceQuad)
                        minimalDistanceQuad = d;
                }
            }
        }

        return Math.sqrt(minimalDistanceQuad);
    }

    protected long getDistance(Point p1, Point p2) {
        return (p2.x - p1.x)*(p2.x - p1.x) +  (p2.y - p1.y)*(p2.y - p1.y);
    }
}
