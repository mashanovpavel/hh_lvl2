package ru.h32.hh.points.solvers;

import ru.h32.hh.points.Point;

import java.util.ArrayList;

public interface Solver {

    public double solve(Point[] points);

}
