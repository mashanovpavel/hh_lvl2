package ru.h32.hh.points.solvers;

import ru.h32.hh.points.generators.RandomGenerator;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class SolverFactory {

    public static Solver getSolver(String type) throws SolverNotFoundException {

        if(type.equals("simplest")) {
             return new SimplestSolver();
        }
        if(type.equals("preparata")) {
            return new PreparataSolver();
        }

        throw new SolverNotFoundException();
    }
}
