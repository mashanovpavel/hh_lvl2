package ru.h32.hh.points;

import ru.h32.hh.points.solvers.Solver;
import ru.h32.hh.points.solvers.SolverFactory;
import ru.h32.hh.points.solvers.SolverNotFoundException;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        if (args.length == 2) {
            try {
                Solver solver = SolverFactory.getSolver(args[0]);
                ArrayList<Point> points = loadFromFile(args[1]);

                Point[] pointsArray = new Point[points.size()];
                pointsArray = points.toArray(pointsArray);

                if(pointsArray.length<=1) {
                    System.out.println("At least two points please");
                }

                double result = solver.solve(pointsArray);

                System.out.println(result);

            } catch (SolverNotFoundException e) {
                System.out.printf("Solver %s not found\n", args[0]);
            } catch (FileNotFoundException e) {
                System.out.println("File not found");
            }
            String filename = args[1];
        } else {
            System.out.println("Use <solver> <file>");
        }
    }

    protected static ArrayList<Point> loadFromFile(String file) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(file));
        ArrayList<Point> list = new ArrayList<Point>();
        while (scanner.hasNextLine()) {
            String s = scanner.nextLine();
            String[] coords = s.split(" ");
            if(coords.length == 2) {
                Point p = new Point(Integer.parseInt(coords[0]), Integer.parseInt(coords[1]));
                list.add(p);
            }
        }
        scanner.close();

        return list;
    }
}
